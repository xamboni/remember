#pragma once

#ifndef BASEPLAYER_H
#define BASEPLAYER_H

#include "materialsystem.h"

class CBasePlayer
{
private:
	Vector pos;
	Vector GetEyePosInternal();
	Vector GetPosInternal();
	Vector m_Velocity;
	QAngle ang;

	float m_flEyeOffset;
	float m_flWalkSpeed;
	float m_flStepSoundTime;
	float m_bOnGround;

	unsigned int keys;

	void HandleFootstep(double curTime, float frameTime, bool bMoving);
	void HandleRecording(int id);

public:
	int playerCount[3] = {0};
	Vector GetPos(); // Retrieve player's position
	void SetPos(const Vector &vecPos); // Set player's position
	void SetPosComponent(int iPart, vec_t value); // Set only one value of the player's position

	QAngle GetEyeAngles(); // Retrieve the angle of the camera
	void SetEyeAngles(const QAngle &eyeAng);
	void SetEyeComponent(int iPart, vec_t value); // Set only one value of the player's view angles

	void SetTall(float flHeight); // Set how tall the player is. In other words, how far up from the ground is the camera?
	float GetTall(); // Retrieve eye height

	void SetWalkSpeed(float flSpeed);
	float GetWalkSpeed();

	void Jump();
	void PlayFootstep();

	void SetVelocity(const Vector &velocity);
	void SetAbsVelocity(const Vector &velocity);
	Vector GetVelocity();

	void KeyPress(UINT vk, BOOL bPressed, int iRepeat, UINT flags);
	void KeyRelease(UINT vk, BOOL bPressed, int iRepeat, UINT flags);

	Vector GetEyePos(); // Get the position of the player's camera. This is GetPos() + Vector(0, 0, GetTall())

	XMMATRIX GetViewMatrix(XMMATRIX *view);
	void Think(float frameTime);

	void Command(unsigned int buttons);
	unsigned int GetButtons();
	void SetButtons(unsigned int buttons);

	void Initialize();

	CBasePlayer();
	~CBasePlayer();
};

#endif // BASEPLAYER_H