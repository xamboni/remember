//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================//

#ifndef BASETYPES_H
#define BASETYPES_H

#include "tier0.h"
#include <vector>
#include "flags.h"
#include <time.h>
#include "sound.h"

typedef void(*function_t)();

//struct SimpleVertex
//{
//	XMFLOAT3 Pos;
//	XMFLOAT2 Tex;
//	XMFLOAT3 Norm;
//};

class SimpleVertex
{
public:
	XMFLOAT3 Pos;
	XMFLOAT2 Tex;
	XMFLOAT3 Normal;
	XMFLOAT3 Tangent;
	XMFLOAT3 BiNormal;
	SimpleVertex()
	{
		Pos = Normal = Tangent = BiNormal = XMFLOAT3(0, 0, 0);
		Tex = XMFLOAT2(0, 0);
	}
	void load(XMFLOAT3 p, XMFLOAT2 tuv, XMFLOAT3 n)
	{
		Pos = p;
		Tex = tuv;
		Normal = n;
	}
};


class ConstantBuffer
{
public:
	ConstantBuffer()
	{
		info = XMFLOAT4(1, 1, 1, 1);
		SunPos = XMFLOAT4(0, 0, 1000, 1);
		DayTimer = XMFLOAT4(0, 0, 0, 0);
	}
	XMMATRIX World;
	XMMATRIX View;
	XMMATRIX Projection;
	XMMATRIX LightViewProjection;
	XMFLOAT4 info;
	XMFLOAT4 CameraPos;
	XMFLOAT4 SunPos;
	XMFLOAT4 DayTimer;
};
class ConstantBufferCS
{
public:
	ConstantBufferCS()
	{
		values = XMFLOAT4(0, 0, 0, 0);
	}
	XMFLOAT4 values;
};

struct VertexBufferData
{
	ID3D11Buffer* vertices;
	int vertexCount;
};

//class ConstantBuffer
//{
//
//public:
//	ConstantBuffer()
//	{
//		info = XMFLOAT4(1, 1, 1, 1);
//	}
//
//	XMMATRIX World;
//	XMMATRIX View;
//	XMMATRIX Projection;
//	XMMATRIX LightView;
//	XMFLOAT4 info;
//	XMFLOAT4 CameraPos;
//	XMFLOAT4 FlashlightPos;
//};

class StopWatchMicro_
{
private:
	LARGE_INTEGER last, frequency;
public:
	StopWatchMicro_()
	{
		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&last);

	}
	long double elapse_micro()
	{
		LARGE_INTEGER now, dif;
		QueryPerformanceCounter(&now);
		dif.QuadPart = now.QuadPart - last.QuadPart;
		long double fdiff = (long double)dif.QuadPart;
		fdiff /= (long double)frequency.QuadPart;
		return fdiff*1000000.;
	}
	long double elapse_milli()
	{
		elapse_micro() / 1000.;
	}
	void start()
	{
		QueryPerformanceCounter(&last);
	}
};

#define  STDCALL				__stdcall
#define  FASTCALL				__fastcall
#define  FORCEINLINE			__forceinline

#ifdef _WIN32
#pragma once
#endif

// Non-macro version of PAD_NUMBER
template <typename T>
inline T AlignValue( T val, uintptr_t alignment )
{
	return (T)( ( (uintptr_t)val + alignment - 1 ) & ~( alignment - 1 ) );
}

// Pad a number so it lies on an N byte boundary.
// So PAD_NUMBER(0,4) is 0 and PAD_NUMBER(1,4) is 4
#define PAD_NUMBER(number, boundary) \
	( ((number) + ((boundary)-1)) / (boundary) ) * (boundary)

// In case this ever changes
#if !defined(M_PI) && !defined(HAVE_M_PI)
#define M_PI			3.14159265358979323846
#endif

// Restirct a value between min and max
template< class T >
T Clamp( T const &val, T const &minVal, T const &maxVal )
{
	if ( val < minVal )
		return minVal;
	else if ( val > maxVal )
		return maxVal;
	else
		return val;
}

// Returns the smaller of two values. Use instead of macro
template< class T >
T Min( T const &val1, T const &val2 )
{
	return val1 < val2 ? val1 : val2;
}

// Returns the max of two values. Use instead of the macro
template< class T >
T Max( T const &val1, T const &val2 )
{
	return val1 > val2 ? val1 : val2;
}

// Increment a number by a value, heading towards target.
inline float Approach(float cur, float target, float inc)
{
	inc = abs(inc);

	if (cur < target)
		return Min(cur + inc, target);

	else if (cur > target)
		return Max(cur - inc, target);

	return target;
}

typedef float vec_t;

inline float FloatMakeNegative( vec_t f )
{
	return -fabsf(f);
}

inline float FloatMakePositive( vec_t f )
{
	return fabsf(f);
}

inline float FloatNegate( vec_t f )
{
	return -f;
}

inline unsigned long& FloatBits( vec_t& f )
{
	return *reinterpret_cast<unsigned long*>(&f);
}

inline unsigned long const& FloatBits( vec_t const& f )
{
	return *reinterpret_cast<unsigned long const*>(&f);
}

inline vec_t BitsToFloat( unsigned long i )
{
	return *reinterpret_cast<vec_t*>(&i);
}

inline bool IsFinite( vec_t f )
{
	return ((FloatBits(f) & 0x7F800000) != 0x7F800000);
}

inline unsigned long FloatAbsBits( vec_t f )
{
	return FloatBits(f) & 0x7FFFFFFF;
}

//template<typename T>
inline float Mod(float x, float y)
{
	if (0. == y)
		return x;

	double m = x - y * floor(x / y);

	// handle boundary cases resulted from floating-point cut off:

	if (y > 0)              // modulo range: [0..y)
	{
		if (m >= y)           // Mod(-1e-16             , 360.    ): m= 360.
			return 0;

		if (m<0)
		{
			if (y + m == y)
				return 0; // just in case...
			else
				return y + m; // Mod(106.81415022205296 , _TWO_PI ): m= -1.421e-14 
		}
	}
	else                    // modulo range: (y..0]
	{
		if (m <= y)           // Mod(1e-16              , -360.   ): m= -360.
			return 0;

		if (m>0)
		{
			if (y + m == y)
				return 0; // just in case...
			else
				return y + m; // Mod(-106.81415022205296, -_TWO_PI): m= 1.421e-14 
		}
	}

	return m;
}

#define FLOAT32_NAN_BITS     (unsigned long)0x7FC00000	// not a number!
#define FLOAT32_NAN          BitsToFloat( FLOAT32_NAN_BITS )
#define VEC_T_NAN FLOAT32_NAN

enum KeyCode
{
	KEY_ESC = 27,
	KEY_SPACE = 32,
	KEY_A = 65,
	KEY_B = 66,
	KEY_C = 67,
	KEY_D = 68,
	KEY_E = 69,
	KEY_F = 70,
	KEY_G = 71,
	KEY_H = 72,
	KEY_I = 73,
	KEY_J = 74,
	KEY_K = 75,
	KEY_L = 76,
	KEY_M = 77,
	KEY_N = 78,
	KEY_O = 79,
	KEY_P = 80,
	KEY_Q = 81,
	KEY_R = 82,
	KEY_S = 83,
	KEY_T = 84,
	KEY_U = 85,
	KEY_V = 86,
	KEY_W = 87,
	KEY_X = 88,
	KEY_Y = 89,
	KEY_Z = 90,
	KEY_LBRACKET = 91,
	KEY_BACKSLASH = 92,
	KEY_RBRACKET = 93,
	KEY_CARET = 94,
	KEY_UNDERSCORE = 95,
	KEY_BACKTICK = 96,
	KEY_LBRACE = 123,
	KEY_PIPE = 124,
	KEY_RBRACE = 125,
	KEY_TILDE = 126,
};

static clock_t START_TIME = clock();

// Returns program uptime in seconds
inline double RunTime()
{
	clock_t curTime = clock();
	double diff = curTime - START_TIME;

	return diff / 1000.0;
}

// Don't touch these
static double framecheck = 0;
static double frametime = 0;
//

// Returns the time in seconds it took render the last frame. FPS can be calculated with 1 / FrameTime()
inline double FrameTime()
{
	return frametime;
}

// Returns a random float value between l and u, inclusive
inline float randomf(float l, float u) {
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = u - l;
	float r = random * diff;
	return l + r;
}

// Return a random integer between l and u, inclusive
inline int random(int l, int u)
{
	double r = (double)(rand() % RAND_MAX) / (double)RAND_MAX;

	return floor(r * (u - l + 1)) + l;
}

#endif // BASETYPES_H
