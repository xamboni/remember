#include "filesys.h"
#include <fstream>
#include <Windows.h>

namespace File
{
	int Find(std::vector<std::string> *files, std::vector<std::string> *folders, const std::string &strFind, bool bUpUpFolders)
	{
		WIN32_FIND_DATAA FindFileData;
		HANDLE hFind;
		unsigned int iFiles = 0;
		hFind = FindFirstFileExA(strFind.c_str(), FindExInfoStandard, &FindFileData, FindExSearchNameMatch, NULL, 0);

		while (hFind != INVALID_HANDLE_VALUE)
		{
			std::string strName = FindFileData.cFileName;

			if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY && folders)
			{
				bool bInclude = true;

				if (bUpUpFolders || ((strName != ".") && (strName != "..")))
				{
					folders->push_back(strName);
					iFiles++;
				}
			}
			else if (files)
			{
				files->push_back(strName);
				iFiles++;
			}

			if (!FindNextFileA(hFind, &FindFileData))
			{
				break;
			}
		}

		FindClose(hFind);
		return iFiles;
	}

	int Exists(const std::string &strFileName)
	{
		struct stat fileStat;
		int err = stat(strFileName.c_str(), &fileStat);

		if (err != 0) { return false; }

		return true;
	}

	bool Read(const std::string &strFileName, std::string &strOut)
	{
		std::ifstream f(strFileName.c_str(), std::ios_base::in);

		if (!f.is_open()) { return false; }

		strOut = std::string((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
		return true;
	}

	bool Write(const std::string & strFileName, const std::string & strOut)
	{
		std::ofstream f(strFileName.c_str(), std::ios_base::out);

		if (!f.is_open()) { return false; }

		f.write(strOut.c_str(), strOut.length());
		f.close();
		return true;
	}

	bool Append(const std::string & strFileName, const std::string & strOut)
	{
		std::ofstream f(strFileName.c_str(), std::ios_base::out | std::ios_base::app);
		if (!f.is_open()) { return false; }

		f.write(strOut.c_str(), strOut.length());
		f.close();
		return true;
	}

	bool IsFolder(const std::string & strFileName)
	{
		struct stat fileStat;
		int err = stat(strFileName.c_str(), &fileStat);

		if (err != 0) { return false; }

		return (fileStat.st_mode & S_IFMT) == S_IFDIR;
	}
}