#include "engine.h"
#include <windowsx.h>
#include "CompileShaderFromFile.h"
#include "timer.h"

bool Load3DS(char *filename, ID3D11Device* g_pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertex_count);

void CEngine::OnTimer(HWND hwnd, UINT id)
{
	Timer::Process();
}
//*************************************************************************
void CEngine::OnKeyUp(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
	switch (vk)
	{
	case 81://q
			//cam.q = 0;

		break;
	case 69://e
			//cam.e = 0; break;
	case 65:
		//cam.a = 0;//a
		break;
	case 68:
		//cam.d = 0;//d
		break;
	case 32: //space
		break;
	case 87:
		//cam.w = 0; //w
		break;
	case 83:
		//cam.s = 0; //s
	default:break;

	}

}

void CEngine::OnKeyDown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{

	switch (vk)
	{
	default:break;
	case 81://q
			//cam.q = 1;
		break;
	case 69://e
			//cam.e = 1;
		break;
	case 65:
		//cam.a = 1;//a
		break;
	case 68:
		//cam.d = 1;//d
		break;
	case 32: //space
		break;
	case 87:
		//cam.w = 1; //w
		break;
	case 83:
		//cam.s = 1; //s
		break;
	case 27:
		PostQuitMessage(0);//escape
		break;

	case 84://t
	{
		static int laststate = 0;

	}
	break;

	}
}

void CEngine::OnLBU(HWND hwnd, int x, int y, UINT keyFlags)
{


}
///////////////////////////////////
//		This Function is called every time the Right Mouse Button is up
///////////////////////////////////
void CEngine::OnRBU(HWND hwnd, int x, int y, UINT keyFlags)
{


}
///////////////////////////////////
//		This Function is called every time the Mouse Moves
///////////////////////////////////
void CEngine::OnMM(HWND hwnd, int x, int y, UINT keyFlags)
{
	//static int holdx = x, holdy = y;
	//static int reset_cursor = 0;



	//RECT rc; 			//rectange structure
	//GetWindowRect(hwnd, &rc); 	//retrieves the window size
	//int border = 20;
	//rc.bottom -= border;
	//rc.right -= border;
	//rc.left += border;
	//rc.top += border;
	//ClipCursor(&rc);

	//if ((keyFlags & MK_LBUTTON) == MK_LBUTTON)
	//	{
	//	}

	//if ((keyFlags & MK_RBUTTON) == MK_RBUTTON)
	//	{
	//	}
	//if (reset_cursor == 1)
	//	{		
	//	reset_cursor = 0;
	//	holdx = x;
	//	holdy = y;
	//	return;
	//	}
	//int diffx = holdx - x;
	//int diffy = holdy - y;
	//float angle_y = (float)diffx / 300.0;
	//float angle_x = (float)diffy / 300.0;
	//cam.rotation.y += angle_y;
	//cam.rotation.x += angle_x;

	//int midx = (rc.left + rc.right) / 2;
	//int midy = (rc.top + rc.bottom) / 2;
	//SetCursorPos(midx, midy);
	//reset_cursor = 1;
}

void CEngine::OnLBD(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
}

LRESULT CALLBACK CEngine::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLBD);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLBU);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMM);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_TIMER, OnTimer);
		HANDLE_MSG(hWnd, WM_KEYDOWN, OnKeyDown);
		HANDLE_MSG(hWnd, WM_KEYUP, OnKeyUp);
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

HRESULT CEngine::CreateVertexBuffer(const string name, const SimpleVertex &vertices, int iNumVertices)
{
	D3D11_BUFFER_DESC bd;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * iNumVertices;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices;

	HRESULT hr = pd3dDevice->CreateBuffer(&bd, &InitData, &VertexBuffers[name].vertices);
	VertexBuffers[name].vertexCount = iNumVertices;

	if (FAILED(hr))
		return hr;
}

// Broken?????????
HRESULT CEngine::CreateShader(const string shaderName, const string fileName)
{
	bool bScreenShader = fileName.find("_screen") != string::npos;
	string vShader = "VS";
	string pShader = "PS";

	if (bScreenShader)
	{
		vShader += "_screen";
		pShader += "_screen";
	}

	LPCSTR vShaderType = vShader.c_str();
	LPCSTR pShaderType = pShader.c_str();

	ID3DBlob* pVSBlob = NULL;

	HRESULT hr = CompileShaderFromFile(s2ws(fileName), s2ws(vShader), L"vs_4_0", &pVSBlob);

	if (FAILED(hr))
	{

		MessageBox(NULL,
			L"Bad shader", L"Error", MB_OK);
		return hr;
	}

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &VertexShaders[shaderName]);
	if (FAILED(hr))
	{
		pVSBlob->Release();
		return hr;
	}

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &pVertexLayout);

	// Create the pixel shader
	hr = CompileShaderFromFile(s2ws(fileName), s2ws(pShader), L"ps_4_0", &pVSBlob);

	if (FAILED(hr))
	{
		string error = "The " + fileName + " shader file is corrupt/missing.";

		MessageBox(NULL,
			L"LOL ERROR", L"Error", MB_OK);
		return hr;
	}

	hr = pd3dDevice->CreatePixelShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &PixelShaders[shaderName]);

	if (FAILED(hr))
		return hr;

	pVSBlob->Release();

	return S_OK;
}

VertexBufferData CEngine::GetVertexBuffer(const string name)
{
	return VertexBuffers[name];
}

HRESULT CEngine::LoadShaders()
{
	vector<string> fileNames = GetFileNamesInDirectory("shaders/*");

	for (int i = 0; i < fileNames.size(); i++)
	{
		if (fileNames[i] == "..") continue;

		string name = fileNames[i];
		size_t lastindex = name.find_last_of(".");

		if (lastindex != string::npos)
			name = name.substr(0, lastindex);

		HRESULT result = CreateShader(name, "shaders/" + fileNames[i]);

		if (FAILED(result))
			return result;
	}
}

ID3D11VertexShader *CEngine::GetVertexShader(const string name)
{
	return VertexShaders[name];
}

ID3D11PixelShader *CEngine::GetPixelShader(const string name)
{
	return PixelShaders[name];
}

HRESULT CEngine::InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	return S_OK;
}

HRESULT CEngine::InitDevice()
{
	return S_OK;
}

// Called when the engine is deleted
void CEngine::CleanupDevice()
{
	//if (pImmediateContext) pImmediateContext->ClearState();
	//if (pSamplerLinear) pSamplerLinear->Release();
	////if (pCBuffer) pCBuffer->Release();
	//if (pVertexLayout) pVertexLayout->Release();
	//if (pDepthStencil) pDepthStencil->Release();
	//if (pDepthStencilView) pDepthStencilView->Release();
	//if (pRenderTargetView) pRenderTargetView->Release();
	//if (pSwapChain) pSwapChain->Release();
	//if (pImmediateContext) pImmediateContext->Release();
	//if (pd3dDevice) pd3dDevice->Release();
}

BOOL CEngine::OnCreate(HWND hwnd, CREATESTRUCT FAR* lpCreateStruct)
{
	RECT rc; 			//rectange structure
	GetWindowRect(hwnd, &rc); 	//retrieves the window size
	int border = 5;
	rc.bottom -= border;
	rc.right -= border;
	rc.left += border;
	rc.top += border;
	ClipCursor(&rc);
	int midx = (rc.left + rc.right) / 2;
	int midy = (rc.top + rc.bottom) / 2;
	SetCursorPos(midx, midy);

	return TRUE;
}

CEngine::CEngine()
{
}

CEngine::~CEngine()
{
	CleanupDevice();
}

HINSTANCE CEngine::GetInst()
{
	return hInst;
}

HWND CEngine::GetWindow()
{
	return hWnd;
}

D3D_DRIVER_TYPE CEngine::GetDriverType()
{
	return driverType;
}

D3D_FEATURE_LEVEL CEngine::GetFeatureLevel()
{
	return featureLevel;
}

ID3D11Device *CEngine::GetDevice()
{
	return pd3dDevice;
}

ID3D11DeviceContext *CEngine::GetDeviceContext()
{
	return pImmediateContext;
}

IDXGISwapChain *CEngine::GetSwapChain()
{
	return pSwapChain;
}

ID3D11RenderTargetView *CEngine::GetRenderTarget()
{
	return pRenderTargetView;
}

ID3D11Texture2D *CEngine::GetStencilBuffer()
{
	return pDepthStencil;
}

ID3D11DepthStencilView *CEngine::GetStencilView()
{
	return pDepthStencilView;
}

ID3D11Buffer* const *CEngine::GetConstantBuffer()
{
	return &pCBuffer;
}

ID3D11SamplerState *CEngine::GetLinearSampler()
{
	return pSamplerLinear;
}

ID3D11SamplerState * CEngine::GetScreenSampler()
{
	return SamplerScreen;
}

XMMATRIX CEngine::GetWorldMatrix()
{
	return World;
}

XMMATRIX CEngine::GetViewMatrix()
{
	return View;
}

XMMATRIX CEngine::GetProjectionMatrix()
{
	return Projection;
}

XMFLOAT4 CEngine::GetMeshColor()
{
	return vMeshColor;
}