#include <fstream>
#include <vector>
#include <time.h>
#include <io.h>
#include "basetypes.h"
using namespace std;

#define MAX 10000

typedef struct vert {

	float x;

	float y;

	float z;

	float normal_x;

	float normal_y;

	float normal_z;

	float tex_x;

	float tex_y;



};


bool loadauh(char *filename, ID3D11Device* g_pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertexcount)
{


	ID3D11Buffer *pVertexBuffer = NULL;
	ifstream myfile;
	string buf;
	int count;
	myfile.open(filename);


	if (myfile.is_open()) {

		while (1) {

			myfile >> buf;

			if (buf == "vertexcount:") {

				myfile >> count;

			}

			if (buf == "vertices") {

				break;

			}

		}

		vert *vertices = new vert[count];
		SimpleVertex *fin = new SimpleVertex[count];
		*vertexcount = count;

		for (int i = 0; i < count; i++) {
			myfile >> vertices[i].x;
			myfile >> vertices[i].y;
			myfile >> vertices[i].z;

			myfile >> vertices[i].normal_x;
			myfile >> vertices[i].normal_y;
			myfile >> vertices[i].normal_z;

			myfile >> buf;
			myfile >> buf;
			myfile >> buf;

			myfile >> buf;
			myfile >> buf;
			myfile >> buf;

			myfile >> vertices[i].tex_x;
			myfile >> vertices[i].tex_y;



			fin[i].Pos = { XMFLOAT3(vertices[i].x, vertices[i].y, vertices[i].z) };



			fin[i].Tex = 	{ XMFLOAT2(vertices[i].tex_x * 3, vertices[i].tex_y * 3) };

			fin[i].Normal = { XMFLOAT3(vertices[i].normal_x, vertices[i].normal_y, vertices[i].normal_z) };


		}

		//initialize d3dx verexbuff:
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(SimpleVertex) * count;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = fin;
		HRESULT hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer);
		if (FAILED(hr))
			return FALSE;
		*ppVertexBuffer = pVertexBuffer;
		return TRUE;




	}


	else {

		return FALSE;

	}

}