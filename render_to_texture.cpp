#include "render_to_texture.h"

UINT TextureClass::GetNumMipLevels()
	{
	UINT numLevels = 1;
	UINT width = w;
	UINT height = h;
	UINT depth = d;
	while (width > 1 && (height == 0 || height > 1) && (depth == 0 || depth > 1))
		{
		width = max(width / 2, 1);
		if (height) height = max(height / 2, 1);
		if (depth) depth = max(depth / 2, 1);
		++numLevels;
		}

	return numLevels;
	}

///////////////////////////////////////////////////////////
bool TextureClass::Initialize_1DTex(ID3D11Device* device, HWND hwnd, DXGI_FORMAT format, int width, bool rtv_, bool srv_, bool uav_, bool dsv_, bool mipmaps, bool regenerate)
	{
	if ((m_texture1D || m_texture2D || m_texture3D))
		{
		if (!regenerate)
			return FALSE;
		else 
			ResetTexturesAndViews();
		}

	if (!rtv_ && !srv_ && mipmaps) mipmaps = false;

	UINT textureWidth;
	if (hwnd) textureWidth = MAXTEX1DSIZE;
	if (width >= 0)	textureWidth = width;
	w = textureWidth;

	rtv = rtv_;
	srv = srv_;
	uav = uav_;
	dsv = dsv_;

	HRESULT result;

	// Set up and create the texture.
	D3D11_TEXTURE1D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.Width = textureWidth;
	textureDesc.MipLevels = (mipmaps ? GetNumMipLevels() : 1); // sets MipLevels to number of logical subdivisions if auto-generating, and 1 if no mipmaps
	textureDesc.ArraySize = 1;
	textureDesc.Format = format;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = (rtv || mipmaps ? D3D11_BIND_RENDER_TARGET : 0) | (srv || mipmaps ? D3D11_BIND_SHADER_RESOURCE : 0) | (uav ? D3D11_BIND_UNORDERED_ACCESS : 0) | (dsv ? D3D11_BIND_DEPTH_STENCIL : 0);
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = (mipmaps ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0);
	result = device->CreateTexture1D(&textureDesc, NULL, &m_texture1D);
	if (FAILED(result))
		return FALSE;

	// Set up and create the render target view.
	if (rtv)
		{
		D3D11_RENDER_TARGET_VIEW_DESC descRTV;
		ZeroMemory(&descRTV, sizeof(descRTV));
		descRTV.Format = format;
		descRTV.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE1D;
		descRTV.Texture1D.MipSlice = 0;
		result = device->CreateRenderTargetView(m_texture1D, &descRTV, &m_renderTargetView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the shader resource view.
	if (srv)
		{
		D3D11_SHADER_RESOURCE_VIEW_DESC descSRV;
		ZeroMemory(&descSRV, sizeof(descSRV));
		descSRV.Format = format;
		descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
		descSRV.Texture1D.MostDetailedMip = 0;
		descSRV.Texture1D.MipLevels = textureDesc.MipLevels;
		result = device->CreateShaderResourceView(m_texture1D, &descSRV, &m_shaderResourceView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the unordered access view.
	if (uav)
		{
		D3D11_UNORDERED_ACCESS_VIEW_DESC descUAV;
		ZeroMemory(&descUAV, sizeof(descUAV));
		descUAV.Format = format;
		descUAV.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE1D;
		descUAV.Texture1DArray.ArraySize = 1;
		descUAV.Texture1DArray.FirstArraySlice = 0;
		descUAV.Texture1DArray.MipSlice = 0;
		result = device->CreateUnorderedAccessView(m_texture1D, &descUAV, &m_unorderedAccessView);
		if (FAILED(result))
			return FALSE;
		}
	
	if (dsv) 
		{
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		descDSV.Format = DXGI_FORMAT_D32_FLOAT;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE1D;
		descDSV.Texture2D.MipSlice = 0;
		descDSV.Flags = 0;
		result = device->CreateDepthStencilView(m_texture2D, &descDSV, &m_depthStencilView);
		if (FAILED(result))
			return FALSE;
		}

	return TRUE;
	}
//////////////////////////////////////////////////////////////
bool TextureClass::Initialize_shadowmap(ID3D11Device* device, DXGI_FORMAT format , int width, int height)
	{
	//create shadow map texture desc
	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;

	//create texture and depth/resource views
	HRESULT result;
	result = device->CreateTexture2D(&texDesc, NULL, &m_texture2D);
	if (FAILED(result))  return false;

	// Create the depth stencil view desc
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	descDSV.Flags = 0;

	result = device->CreateDepthStencilView(m_texture2D, &descDSV, &m_depthStencilView);
	if (FAILED(result))  return false;

	//create shader resource view desc
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = texDesc.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;

	result = device->CreateShaderResourceView(m_texture2D, &srvDesc, &m_shaderResourceView);
	if (FAILED(result)) return false;

	}
///////////////////////////////////////////////////////////
bool TextureClass::Initialize_2DTex(ID3D11Device* device, HWND hwnd, DXGI_FORMAT format, int width, int height, bool rtv_, bool srv_, bool uav_, bool dsv_, bool mipmaps, bool regenerate)
	{
	if ((m_texture1D || m_texture2D || m_texture3D))
		{
		if (!regenerate)
			return FALSE;
		else
			ResetTexturesAndViews();
		}

	if (!rtv_ && !srv_ && mipmaps) mipmaps = false;

	RECT rc;
	UINT textureWidth;
	UINT textureHeight;
	if (hwnd)
		{
		GetClientRect(hwnd, &rc);
		textureWidth = rc.right - rc.left;
		textureHeight = rc.bottom - rc.top;
		}
	if (width >= 0)	textureWidth = width;
	if (height >= 0) textureHeight = height;
	w = textureWidth;
	h = textureHeight;

	rtv = rtv_;
	srv = srv_;
	uav = uav_;
	dsv = dsv_;

	HRESULT result;

	// Set up and create the texture.
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.Width = textureWidth;
	textureDesc.Height = textureHeight;
	textureDesc.MipLevels = (mipmaps ? GetNumMipLevels() : 1);
	textureDesc.ArraySize = 1;
	textureDesc.Format = format;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = (rtv || mipmaps ? D3D11_BIND_RENDER_TARGET : 0) | (srv || mipmaps ? D3D11_BIND_SHADER_RESOURCE : 0) | (uav ? D3D11_BIND_UNORDERED_ACCESS : 0) | (dsv ? D3D11_BIND_DEPTH_STENCIL : 0);
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = (mipmaps ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0);
	result = device->CreateTexture2D(&textureDesc, NULL, &m_texture2D);
	if (FAILED(result))
		return FALSE;

	// Set up and create the render target view.
	if (rtv)
		{
		D3D11_RENDER_TARGET_VIEW_DESC descRTV;
		ZeroMemory(&descRTV, sizeof(descRTV));
		descRTV.Format = format;
		descRTV.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		descRTV.Texture2D.MipSlice = 0;
		result = device->CreateRenderTargetView(m_texture2D, &descRTV, &m_renderTargetView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the shader resource view.
	if (srv)
		{
		D3D11_SHADER_RESOURCE_VIEW_DESC descSRV;
		ZeroMemory(&descSRV, sizeof(descSRV));
		descSRV.Format = format;
		descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		descSRV.Texture2D.MostDetailedMip = 0;
		descSRV.Texture2D.MipLevels = textureDesc.MipLevels;
		result = device->CreateShaderResourceView(m_texture2D, &descSRV, &m_shaderResourceView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the unordered access view.
	if (uav)
		{
		D3D11_UNORDERED_ACCESS_VIEW_DESC descUAV;
		ZeroMemory(&descUAV, sizeof(descUAV));
		descUAV.Format = format;
		descUAV.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		descUAV.Texture2DArray.ArraySize = 1;
		descUAV.Texture2DArray.FirstArraySlice = 0;
		descUAV.Texture2DArray.MipSlice = 0;
		result = device->CreateUnorderedAccessView(m_texture2D, &descUAV, &m_unorderedAccessView);
		if (FAILED(result))
			return FALSE;
		}

	if (dsv)
		{
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		descDSV.Format = format;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
		descDSV.Texture2D.MipSlice = 0;
		descDSV.Flags = 0;
		result = device->CreateDepthStencilView(m_texture2D, &descDSV, &m_depthStencilView);
		if (FAILED(result))
			return FALSE;
		}

	return true;
	}

///////////////////////////////////////////////////////////
bool TextureClass::Initialize_3DTex(ID3D11Device* device, DXGI_FORMAT format, int width, int height, int depth, bool rtv_, bool srv_, bool uav_, bool mipmaps, bool regenerate)
	{
	if ((m_texture1D || m_texture2D || m_texture3D))
		{
		if (!regenerate)
			return FALSE;
		else 
			ResetTexturesAndViews();
		}

	if (!rtv_ && !srv_ && mipmaps) mipmaps = false;

	w = (width >= 0 ? width : 128);
	h = (height >= 0 ? height : 128);
	d = (depth >= 0 ? depth : 128);

	rtv = rtv_;
	srv = srv_;
	uav = uav_;

	HRESULT result;
	D3D11_TEXTURE3D_DESC textureDesc;

	// Set up and create the texture.
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.Depth = depth;
	textureDesc.MipLevels = (mipmaps ? GetNumMipLevels() : 1); // sets MipLevels to number of logical subdivisions if auto-generating, and 1 if no mipmaps
	textureDesc.Format = format;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = (rtv || mipmaps ? D3D11_BIND_RENDER_TARGET : 0) | (srv || mipmaps ? D3D11_BIND_SHADER_RESOURCE : 0) | (uav ? D3D11_BIND_UNORDERED_ACCESS : 0);
	textureDesc.CPUAccessFlags = NULL;
	textureDesc.MiscFlags = (mipmaps ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0);
	result = device->CreateTexture3D(&textureDesc, NULL, &m_texture3D);
	if (FAILED(result))
		return FALSE;

	// Set up and create the description of the render target view.
	if (rtv)
		{
		D3D11_RENDER_TARGET_VIEW_DESC descRTV;
		ZeroMemory(&descRTV, sizeof(descRTV));
		descRTV.Format = format;
		descRTV.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE3D;
		descRTV.Texture3D.MipSlice = 0;
		result = device->CreateRenderTargetView(m_texture3D, &descRTV, &m_renderTargetView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the shader resource view.
	if (srv)
		{
		D3D11_SHADER_RESOURCE_VIEW_DESC descSRV;
		ZeroMemory(&descSRV, sizeof(descSRV));
		descSRV.Format = format;
		descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
		descSRV.Texture3D.MostDetailedMip = 0;
		descSRV.Texture3D.MipLevels = textureDesc.MipLevels;
		result = device->CreateShaderResourceView(m_texture3D, &descSRV, &m_shaderResourceView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the unordered access view.
	if (uav)
		{
		D3D11_UNORDERED_ACCESS_VIEW_DESC descUAV;
		ZeroMemory(&descUAV, sizeof(descUAV));
		descUAV.Format = format;
		descUAV.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
		descUAV.Texture3D.MipSlice = 0;
		descUAV.Texture3D.FirstWSlice = 0;
		descUAV.Texture3D.WSize = depth;
		result = device->CreateUnorderedAccessView(m_texture3D, &descUAV, &m_unorderedAccessView);
		if (FAILED(result))			
			return FALSE;
		}

	return TRUE;
	}

///////////////////////////////////////////////////////////
bool TextureClass::Initialize_staging(ID3D11Device* device)
	{
	HRESULT hr;

	D3D11_TEXTURE1D_DESC texture1dDesc;
	D3D11_TEXTURE2D_DESC texture2dDesc;
	D3D11_TEXTURE3D_DESC texture3dDesc;

	if(!m_texture1D && !m_texture2D && !m_texture3D)
		return FALSE;
	else {
		if (m_texture1D && !m_staging1D) {
			m_texture1D->GetDesc(&texture1dDesc);
			texture1dDesc.MipLevels = 1;
			texture1dDesc.Usage = D3D11_USAGE_STAGING;
			texture1dDesc.BindFlags = 0;
			texture1dDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			texture1dDesc.MiscFlags = 0;
			hr = device->CreateTexture1D(&texture1dDesc, NULL, &m_staging1D);
			if (FAILED(hr))
				return FALSE;
			}
		else if (m_texture2D && !m_staging2D)
			{
			m_texture2D->GetDesc(&texture2dDesc);
			texture2dDesc.MipLevels = 1;
			texture2dDesc.Usage = D3D11_USAGE_STAGING;
			texture2dDesc.BindFlags = 0;
			texture2dDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			texture2dDesc.MiscFlags = 0;
			hr = device->CreateTexture2D(&texture2dDesc, NULL, &m_staging2D);
			if (FAILED(hr))
				return FALSE;
			}
		else if (m_texture3D && !m_staging3D)
			{
			m_texture3D->GetDesc(&texture3dDesc);
			texture3dDesc.MipLevels = 1;
			texture3dDesc.Usage = D3D11_USAGE_STAGING;
			texture3dDesc.BindFlags = 0;
			texture3dDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			texture3dDesc.MiscFlags = 0;
			hr = device->CreateTexture3D(&texture3dDesc, NULL, &m_staging3D);
			if (FAILED(hr))
				return FALSE;
			}
		}

	return TRUE;
	}

///////////////////////////////////////////////////////////
void TextureClass::ResetTexturesAndViews()
	{
	srv = uav = rtv = dsv = false;
	w = h = d = 0;
	if (m_texture1D) { m_texture1D->Release(); m_texture1D = NULL; }
	if (m_texture2D) { m_texture2D->Release(); m_texture2D = NULL; }
	if (m_texture3D) { m_texture3D->Release(); m_texture3D = NULL; }
	if (m_staging1D) { m_staging1D->Release(); m_staging1D = NULL; }
	if (m_staging2D) { m_staging2D->Release(); m_staging2D = NULL; }
	if (m_staging3D) { m_staging3D->Release(); m_staging3D = NULL; }
	if (m_shaderResourceView) { m_shaderResourceView->Release();  m_shaderResourceView = NULL; }
	if (m_renderTargetView) { m_renderTargetView->Release();  m_renderTargetView = NULL; }
	if (m_unorderedAccessView) { m_unorderedAccessView->Release();  m_unorderedAccessView = NULL; }
	if (m_depthStencilView) { m_depthStencilView->Release();  m_depthStencilView = NULL; }
	}