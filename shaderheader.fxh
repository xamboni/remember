#include "commonheader.h"

uint3 dto3d(uint idx)
	{
	int bps = BRICKPOOLSIZE / 3;
	int x = idx % bps;
	int y = (idx / bps) % bps;
	int z = idx / (bps*bps);

	return uint3(x * 3, y * 3, z * 3);
	}

uint power_2(uint exp)
	{
	return 1 << exp;
	}

float remap(float value, float inMin, float inMax, float outMin, float outMax)
{
	return outMin + (((value - inMin) / (inMax - inMin)) * (outMax - outMin));
}

/*
Octree branches [10 elements per division]
//	[0 - 7] octree nodes
//	[8] brick pool index
//	[9] binary index

Octree last level nodes [6 elements per node]
//  [0] brick pool index
//	[1] binary index
//  [2 - 4] RGB color information
//	[5] pixel count

Count [50 element array]
//	[0] - next vfl index / total vfl count
//	[1] - beginning of current octree level
//	[2] - next octree index / total octree array element count - static
//	[3] - total octree subdivision count
//	[4] - next octree index / total octree array element count - dynamic
//	[5] - total pixel count
//	[6] - next brickbuffer index / total brickbuffer count
//	[7-9] - free, use for debugbuffer
//	[10 - 19] total octree array element count for each level
//	[20 - 29] total brickbuffer count for each level

Octree subdivision binary indices
// 000 ... 0 ... up / left / front
// 001 ... 1 ... up / left / back
// 010 ... 2 ... up / right / front
// 011 ... 3 ... up / right / back
// 100 ... 4 ... down / left / front
// 101 ... 5 ... down / left / back
// 110 ... 6 ... down / right / front
// 111 ... 7 ... down / right / back
*/