#pragma once
#include "groundwork.h"

XMFLOAT3 operator+(const XMFLOAT3 lhs, XMFLOAT3 rhs);
XMFLOAT3 operator-(const XMFLOAT3 lhs, XMFLOAT3 rhs);
XMFLOAT2 operator+(const XMFLOAT2 lhs, XMFLOAT2 rhs);
XMFLOAT2 operator-(const XMFLOAT2 lhs, XMFLOAT2 rhs);
XMFLOAT3 operator*(const XMMATRIX &lhs, XMFLOAT3 rhs);
XMFLOAT3 operator*(XMFLOAT3 rhs, const XMMATRIX &lhs);
XMFLOAT3 operator*(const XMFLOAT3 lhs, float rhs);
XMFLOAT3 operator*(float rhs, const XMFLOAT3 lhs);
XMFLOAT3 normalize(XMFLOAT3 in);

float length(XMFLOAT3 v);
float length(XMFLOAT2 v);
float angle(XMFLOAT3 a, XMFLOAT3 b);
bool operator==(const XMFLOAT3 lhs, XMFLOAT3 rhs);
float dot(XMFLOAT3 a, XMFLOAT3 b);
XMFLOAT3 cross(XMFLOAT3 a, XMFLOAT3 b);
float dot(XMFLOAT2 a, XMFLOAT2 b);
XMFLOAT3 XMFLOAT3CompMultiply(XMFLOAT3 a, XMFLOAT3 b);
XMFLOAT3 XMFLOAT3Lint(XMFLOAT3 a, XMFLOAT3 b, float t);
XMFLOAT3 mul(XMFLOAT3 v, XMMATRIX &M);
XMFLOAT2 v32(XMFLOAT3 v);
XMFLOAT3 v23(XMFLOAT2 v, float z);
bool line_line_intersection_segments(XMFLOAT2 p1, XMFLOAT2 p2, XMFLOAT2 p3, XMFLOAT2 p4, XMFLOAT2 *erg);