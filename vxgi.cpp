
#include "vxgi.h"
#include "materialsystem.h"


void vxgi_::init(HWND hWnd, ID3D11Device* d3dDevice)
	{
	HRESULT hr = S_OK;
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	D3DX11CreateShaderResourceViewFromFile(d3dDevice, L"materials/maskwide.png", NULL, NULL, &flashlightMask, NULL);

	RECT rc;
	GetClientRect(hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	// Create depth stencil texture
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = d3dDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
	if (FAILED(hr))
		return;

	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D32_FLOAT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	hr = d3dDevice->CreateDepthStencilView(pDepthStencil, &descDSV, &pDepthStencilView);
	if (FAILED(hr))
		return;

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = d3dDevice->CreateSamplerState(&sampDesc, &pSamplerLinear);
	if (FAILED(hr))
		return;

	viewport_octree.Width = (FLOAT)256;
	viewport_octree.Height = (FLOAT)144;
	viewport_octree.MinDepth = 0.0f;
	viewport_octree.MaxDepth = 1.0f;
	viewport_octree.TopLeftX = 0;
	viewport_octree.TopLeftY = 0;

	// Create orthogonal view matrices
	XMVECTOR Eye = XMVectorSet(0.0f, 17.0f, -100.0f, 0.0f);//camera position
	XMVECTOR At = XMVectorSet(0.0f, 17, 0.0f, 0.0f);//look at
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);// normal vector on at vector (always up)
	View_front = XMMatrixLookAtLH(Eye, At, Up);

	Eye = XMVectorSet(0.0f, 100.0f, 0.0f, 0.0f);//camera position
	At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);//look at
	Up = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);// normal vector on at vector (always up)
	View_top = XMMatrixLookAtLH(Eye, At, Up);

	Eye = XMVectorSet(0.0f, 20.0f, -100.0f, 0.0f);//camera position
	At = XMVectorSet(0.0f, 20.0f, 0.0f, 0.0f);//look at
	Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);// normal vector on at vector (always up)
	View_left = XMMatrixLookAtLH(Eye, At, Up);

	//Create the constant buffer
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = d3dDevice->CreateBuffer(&bd, NULL, &pCBuffer);
	if (FAILED(hr))
		return;

	// Create the constant buffer for the CS
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBufferCS);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = d3dDevice->CreateBuffer(&bd, NULL, &pCBufferCS);
	if (FAILED(hr))
		return;

	// Create projection matrix
	Projection_ortho = XMMatrixOrthographicLH(256, (FLOAT)144, -100.01f, 10000.0f) * XMMatrixScaling(2.8, 2.8, 2.8);

	// Create voxel vertex buffer
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * voxel.anz;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = voxel.v;
	hr = d3dDevice->CreateBuffer(&bd, &InitData, &voxel.vbuffer);
	if (FAILED(hr))
		return;

	SimpleVertex vertices_screen[6];
	vertices_screen[0].load(XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(0, 0), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[1].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1, 0), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[2].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0, 1), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[3].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0, 1), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[4].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1, 0), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[5].load(XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(1, 1), XMFLOAT3(0.0f, 0.0f, 1.0f));

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * 6;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices_screen;
	hr = d3dDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer);
	if (FAILED(hr))
		return;

	/////////// Shaders ///////////
	ID3DBlob* pVSBlob = NULL;
	ID3DBlob* pGSBlob = NULL;
	ID3DBlob* pPSBlob = NULL;

	/////////step 1 shaders///////////////
	// VFL vertex shader
	hr = CompileShaderFromFile(L"VFL.fx", "VS", "vs_5_0", &pVSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &pVFLVS);
	if (FAILED(hr))
		{
		pVSBlob->Release();
		return;
		}

	// VFL pixel shader
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"VFL.fx", "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pVFLPS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	/////////step 2 shaders///////////////
	// Compute shader starting stage
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute.fx", "CSstart", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pStartCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	// Compute shader flagging stage
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute.fx", "CSflag", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pFlagCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	// Compute shader building stage
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute.fx", "CSbuild", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pBuildCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute.fx", "CSSetLastLvl", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pSetLastLvlCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Step 3 shaders ///////////////
	// Compute shader color exchanging from kids to octree element stage
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute color xchange.fx", "CSkids_color", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pKidsColorExchangeCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Step 4 shaders ///////////////
	// Compute shader neighboring stage
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute neighbor.fx", "NeighborIndexing", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pNeighborCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Step 5 shaders ///////////////
	// Compute shader color exchanging stage
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute color xchange.fx", "ColorExchange", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pColorExchangeCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Step 6 shaders ///////////////
	// Deferred pixel shader
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"deferred.fx", "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pDeferredPS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Step 7 shaders ///////////////
	// Scene regular vertex shader
	hr = CompileShaderFromFile(L"scene.fx", "VS", "vs_5_0", &pVSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &pSceneVS);
	if (FAILED(hr))
		{
		pVSBlob->Release();
		return;
		}

	// Scene screen space vertex shader
	hr = CompileShaderFromFile(L"scene.fx", "VS_scene", "vs_5_0", &pVSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &pSceneScreenVS);
	if (FAILED(hr))
		{
		pVSBlob->Release();
		return;
		}

	// Scene/sponza pixel shader
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"scene.fx", "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pScenePS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	//deferred shadowing pixel shader
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"deferred_shadow.fx", "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pDeferredShadowPS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Voxel shaders ///////////////
	// Voxel vertex shader
	hr = CompileShaderFromFile(L"voxel.fx", "VS", "vs_5_0", &pVSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &pVoxelVS);
	if (FAILED(hr))
		{
		pVSBlob->Release();
		return;
		}

	// Voxel geometry shader
	hr = CompileShaderFromFile(L"voxel.fx", "GS", "gs_5_0", &pGSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateGeometryShader(pGSBlob->GetBufferPointer(), pGSBlob->GetBufferSize(), NULL, &pVoxelGS);
	pGSBlob->Release();
	if (FAILED(hr))
		return;

	// Voxel pixel shader
	hr = CompileShaderFromFile(L"voxel.fx", "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pVoxelPS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Buffer resetting shader ///////////////
	// Compute shader for clearing UAVs
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"compute.fx", "CSclear", "cs_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateComputeShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pClearCS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Regular lighting comparison shader ///////////////
	// Screen pixel shader
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"scene.fx", "PScompare", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
	}
	hr = d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &g_pScreenPScompare);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Writable shader targets ///////// 
	Positions.Initialize_2DTex(d3dDevice, hWnd, DXGI_FORMAT_R32G32B32A32_FLOAT, -1, -1, true, true, false, false, false, false);
	Normals.Initialize_2DTex(d3dDevice, hWnd, DXGI_FORMAT_R8G8B8A8_UNORM, -1, -1, true, true, false, false, false, false);
	Tangents.Initialize_2DTex(d3dDevice, hWnd, DXGI_FORMAT_R8G8B8A8_UNORM, -1, -1, true, true, false, false, false, false);
	DeferredShadow.Initialize_2DTex(d3dDevice, hWnd, DXGI_FORMAT_R8G8B8A8_UNORM, -1, -1, true, true, false, false, false, false);
	Diffuse.Initialize_2DTex(d3dDevice, hWnd, DXGI_FORMAT_R8G8B8A8_UNORM, -1, -1, true, true, false, false, false, false);
	ScreenOutput.Initialize_2DTex(d3dDevice, hWnd, DXGI_FORMAT_R8G8B8A8_UNORM, -1, -1, true, true, false);
	BrickBuffer.Initialize_3DTex(d3dDevice, DXGI_FORMAT_R32_FLOAT, BRICKPOOLSIZE, BRICKPOOLSIZE, BRICKPOOLSIZE);

	Octree.Initialize_buffer(d3dDevice, sizeof(unsigned int), BUFFERSIZE);
	VFL.Initialize_buffer(d3dDevice, sizeof(XMFLOAT3), BUFFERSIZE, false, false, true);
	VFLcolors.Initialize_buffer(d3dDevice, sizeof(XMFLOAT4), BUFFERSIZE, false, false, true);
	const_count.Initialize_buffer(d3dDevice, sizeof(unsigned int), COUNTSIZE);
	NeighborBuffer.Initialize_buffer(d3dDevice, sizeof(unsigned int), BUFFERSIZE_NB);
	CSDebugBuffer.Initialize_buffer(d3dDevice, sizeof(unsigned int), BUFFERSIZE);

	}
//***********************************************************************************************************************
void vxgi_::Reset_CS(ID3D11DeviceContext* pImmediateContext)
	{
	ID3D11UnorderedAccessView*	ppUAVssNULL[7] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL };
	ID3D11ShaderResourceView*	ppSRVsNULL[10] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
	ID3D11Buffer*				ppBuffsNULL[10] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
	unsigned int uavCounts[7] = { -1,-1,-1,-1,-1,-1,-1 };
	int actualnum_uav = 7;	//max count of uavs = read/write stuff
	int actualnum_srv = 1;	//max count of shader resource views = textures for input
	int actualnum_buffs = 1;//count of constant buffer = what you get from the C++ side

	if (actualnum_uav)		pImmediateContext->CSSetUnorderedAccessViews(1, actualnum_uav, ppUAVssNULL, uavCounts);
	if (actualnum_srv)		pImmediateContext->CSSetShaderResources(0, actualnum_srv, ppSRVsNULL);
	if (actualnum_buffs)	pImmediateContext->CSSetConstantBuffers(0, actualnum_buffs, ppBuffsNULL);

	//actualnum_buffs = actualnum_uav = actualnum_srv = 0;
	pImmediateContext->CSSetShader(NULL, NULL, 0);
	}
//**********************************************************************************************
void vxgi_::reset_all(ID3D11DeviceContext* pImmediateContext) {
	unsigned int ClearColorUAV[4] = { 0 };
	pImmediateContext->ClearUnorderedAccessViewUint(BrickBuffer.GetUnorderedAccessView(), &ClearColorUAV[0]);

	ID3D11UnorderedAccessView* uav[7] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL };
	// -1 to reset internal element count

	uav[0] = Octree.GetUnorderedAccessView();
	uav[1] = VFL.GetUnorderedAccessView();
	uav[2] = VFLcolors.GetUnorderedAccessView();
	uav[3] = const_count.GetUnorderedAccessView();
	uav[5] = NeighborBuffer.GetUnorderedAccessView();
	uav[6] = CSDebugBuffer.GetUnorderedAccessView();

	pImmediateContext->CSSetUnorderedAccessViews(1, 7, uav, nullptr);

	// reset shaders (if they are set)
	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	pImmediateContext->GSSetShader(NULL, NULL, 0);

	// set up constant buffer
	ConstantBufferCS constantbufferCS;
	constantbufferCS.values = XMFLOAT4(1, 0, 0, 0);
	pImmediateContext->UpdateSubresource(pCBufferCS, 0, NULL, &constantbufferCS, 0, 0);
	pImmediateContext->CSSetConstantBuffers(0, 1, &pCBufferCS);

	// clear uavs
	pImmediateContext->CSSetShader(pClearCS, NULL, 0);
	pImmediateContext->Dispatch(1, 1, 1);

	Reset_CS(pImmediateContext);
	}
//*********************************************************************
void vxgi_::render_1_of_8_render_into_voxel_fragment_list(ID3D11DeviceContext* pImmediateContext, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, float time, shadow_mapping_ *shadow_mapping, void(*render)(ID3D11DeviceContext*, float, ID3D11Buffer*, ConstantBuffer &))
	{
	D3D11_VIEWPORT viewport_normal;
	UINT viewport_num = 1;

	pImmediateContext->RSGetViewports(&viewport_num, &viewport_normal);
	pImmediateContext->RSSetViewports(1, &viewport_octree);

	ID3D11UnorderedAccessView* uav[7] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL };

	uav[0] = VFL.GetUnorderedAccessView();
	uav[1] = const_count.GetUnorderedAccessView();
	uav[2] = VFLcolors.GetUnorderedAccessView();

	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);

	pImmediateContext->OMSetRenderTargetsAndUnorderedAccessViews(0, nullptr, 0, 1, 3, uav, 0);

	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pImmediateContext->VSSetShader(pVFLVS, NULL, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->VSSetSamplers(0, 1, &pSamplerLinear);

	ID3D11ShaderResourceView* texture = shadow_mapping->get_output()->GetShaderResourceView();
	pImmediateContext->PSSetShaderResources(1, 1, &texture);
	pImmediateContext->PSSetShader(pVFLPS, NULL, 0);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetSamplers(0, 1, &pSamplerLinear);
	ID3D11SamplerState* samplepoint = shadow_mapping->get_point_sampler();
	pImmediateContext->PSSetSamplers(1, 1, &samplepoint);
	
	//render from top
	ConstantBuffer cb;
	cb.World = XMMatrixTranspose(l_world);
	cb.Projection = XMMatrixTranspose(Projection_ortho*XMMatrixScaling(0.85, 0.85, 0.85));
	cb.View = XMMatrixTranspose(View_top);
	cb.LightViewProjection = XMMatrixTranspose(shadow_mapping->get_light_VP());
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);
	render(pImmediateContext, time, pCBuffer, cb);

	//render from front
	cb.Projection = XMMatrixTranspose(Projection_ortho);
	cb.View = XMMatrixTranspose(View_front);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);
	render(pImmediateContext, time, pCBuffer, cb);

	//render from left
	cb.Projection = XMMatrixTranspose(Projection_ortho*XMMatrixScaling(0.85, 0.85, 0.85));
	cb.View = XMMatrixTranspose(View_left);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);
	render(pImmediateContext, time, pCBuffer,cb);

	pSwapChain->Present(0, 0);

	// Clear VS and PS resources
	ID3D11RenderTargetView* RT;
	RT = 0;
	for (int i = 0; i < 7; i++)
		uav[i] = 0;
	pImmediateContext->OMSetRenderTargetsAndUnorderedAccessViews(1, &RT, 0, 1, 7, uav, 0);

	ID3D11ShaderResourceView* srvs[10] = { 0 };
	pImmediateContext->VSSetShaderResources(0, 10, srvs);
	pImmediateContext->PSSetShaderResources(0, 10, srvs);

	pImmediateContext->RSSetViewports(viewport_num, &viewport_normal);

	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	}
//***********************************************************************************************************************
void vxgi_::render_2_of_8_build_octree(ID3D11DeviceContext* pImmediateContext)
	{	
	ID3D11UnorderedAccessView* uav[7] = { NULL, NULL, NULL,NULL, NULL, NULL ,NULL };
	unsigned int uavCounts[7] = { -1,-1,-1,-1,-1,-1,-1 }; //has to be -1 for structured buffers

	uav[0] = Octree.GetUnorderedAccessView();
	uav[1] = VFL.GetUnorderedAccessView();
	uav[2] = VFLcolors.GetUnorderedAccessView();
	uav[3] = const_count.GetUnorderedAccessView();
	uav[4] = BrickBuffer.GetUnorderedAccessView();
	uav[5] = NeighborBuffer.GetUnorderedAccessView();

	pImmediateContext->CSSetUnorderedAccessViews(1, 7, uav, uavCounts);
	
	// set up constant buffer
	ConstantBufferCS constantbufferCS;
	constantbufferCS.values = XMFLOAT4(0, 0, 0, 0);
	pImmediateContext->UpdateSubresource(pCBufferCS, 0, NULL, &constantbufferCS, 0, 0);
	pImmediateContext->CSSetConstantBuffers(0, 1, &pCBufferCS);

	pImmediateContext->CSSetShader(pStartCS, NULL, 0);
	pImmediateContext->Dispatch(1, 1, 1);

	for (int i = 0; i <= maxlevel; i++)
		{
		//passing the level
		constantbufferCS.values = XMFLOAT4((float)i + EPS, 0, 0, 0);
		pImmediateContext->UpdateSubresource(pCBufferCS, 0, NULL, &constantbufferCS, 0, 0);
		pImmediateContext->CSSetConstantBuffers(0, 1, &pCBufferCS);

		//flagging
		pImmediateContext->CSSetShader(pFlagCS, NULL, 0);
		pImmediateContext->Dispatch(1, 1, 1);

		if (i == maxlevel) break;
		//building
		pImmediateContext->CSSetShader(pBuildCS, NULL, 0);
		pImmediateContext->Dispatch(1, 1, 1);
		}
	pImmediateContext->CSSetShader(pSetLastLvlCS, NULL, 0);
	pImmediateContext->Dispatch(1, 1, 1);

	Reset_CS(pImmediateContext);
	}
//***********************************************************************************************************************
void vxgi_::render_3_of_8_collect_kids_color(ID3D11DeviceContext* pImmediateContext)
	{
	ID3D11UnorderedAccessView* uav[7] = { NULL, NULL, NULL,NULL, NULL, NULL ,NULL };
	unsigned int uavCounts[7] = { -1,-1,-1,-1,-1,-1,-1 }; //has to be -1 for structured buffers

	uav[0] = BrickBuffer.GetUnorderedAccessView();
	uav[1] = NeighborBuffer.GetUnorderedAccessView();
	uav[2] = const_count.GetUnorderedAccessView();
	uav[3] = CSDebugBuffer.GetUnorderedAccessView();
	uav[4] = Octree.GetUnorderedAccessView();

	ConstantBufferCS constantbufferCS;
	constantbufferCS.values = XMFLOAT4(0, 0, 0, 0);
	pImmediateContext->CSSetUnorderedAccessViews(1, 5, uav, uavCounts);
	pImmediateContext->CSSetShader(pKidsColorExchangeCS, NULL, 0);
	for (int i = maxlevel - 1; i >= 0; i--)
		{
		//passing the level
		constantbufferCS.values = XMFLOAT4((float)i + EPS, 0, 0, 0);
		pImmediateContext->UpdateSubresource(pCBufferCS, 0, NULL, &constantbufferCS, 0, 0);
		pImmediateContext->CSSetConstantBuffers(0, 1, &pCBufferCS);
		pImmediateContext->Dispatch(1, 1, 1);
		}

	Reset_CS(pImmediateContext);
	}
//***********************************************************************************************************************
void vxgi_::render_4_of_8_build_neighbor_info(ID3D11DeviceContext* pImmediateContext)
	{
	ID3D11UnorderedAccessView* uav[7] = { NULL, NULL, NULL,NULL, NULL, NULL ,NULL };
	unsigned int uavCounts[7] = { 0 }; //-1 to clear counts, 0 to keep counts, anything else to set counts?


	uav[0] = NeighborBuffer.GetUnorderedAccessView();
	uav[1] = const_count.GetUnorderedAccessView();
	uav[2] = Octree.GetUnorderedAccessView();
	uav[3] = CSDebugBuffer.GetUnorderedAccessView();


	pImmediateContext->CSSetUnorderedAccessViews(1, 4, uav, uavCounts);
	pImmediateContext->CSSetShader(pNeighborCS, NULL, 0);
	ConstantBufferCS constantbufferCS;
	constantbufferCS.values = XMFLOAT4(0, 0, 0, 0);
	for (int i = 0; i <= maxlevel; i++)
		{
		//passing the level
		constantbufferCS.values = XMFLOAT4((float)i + EPS, 0, 0, 0);
		pImmediateContext->UpdateSubresource(pCBufferCS, 0, NULL, &constantbufferCS, 0, 0);
		pImmediateContext->CSSetConstantBuffers(0, 1, &pCBufferCS);
		pImmediateContext->Dispatch(1, 1, 1);
		}

	Reset_CS(pImmediateContext);
	}
//***********************************************************************************************************************
void vxgi_::render_5_of_8_exchange_color(ID3D11DeviceContext* pImmediateContext)
	{
	ID3D11UnorderedAccessView* BrickBuffer_UAV = BrickBuffer.GetUnorderedAccessView();
	ID3D11UnorderedAccessView* uav[7] = { NULL, NULL, NULL,NULL, NULL, NULL ,NULL };
	unsigned int uavCounts[7] = { -1,-1,-1,-1,-1,-1,-1 }; //has to be -1 for structured buffers

	uav[0] = BrickBuffer.GetUnorderedAccessView();
	uav[1] = NeighborBuffer.GetUnorderedAccessView();
	uav[2] = const_count.GetUnorderedAccessView();
	uav[3] = CSDebugBuffer.GetUnorderedAccessView();

	pImmediateContext->CSSetUnorderedAccessViews(1, 4, uav, uavCounts);

	pImmediateContext->CSSetShader(pColorExchangeCS, NULL, 0);
	pImmediateContext->Dispatch(1, 1, 1);

	Reset_CS(pImmediateContext);
	}

//**********************************************************************************************
void vxgi_::render_6_of_8_render_scene_into_RT(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection, void(*render)(ID3D11DeviceContext*, float, ID3D11Buffer*, ConstantBuffer &))
	{
	float ClearColor[3] = { 0.0f, 0.0f, 0.0f }; // red, green, blue
	ID3D11RenderTargetView*			RenderTargets[4];
	RenderTargets[0] = Positions.GetRenderTargetView();
	RenderTargets[1] = Normals.GetRenderTargetView();
	RenderTargets[2] = Tangents.GetRenderTargetView();
	RenderTargets[3] = Diffuse.GetRenderTargetView();
	
	// Clear render target & shaders, set render target & primitive topology
	for (int ii = 0; ii < 4; ii++) {
		pImmediateContext->ClearRenderTargetView(RenderTargets[ii], ClearColor);
		}
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	pImmediateContext->OMSetRenderTargets(4, RenderTargets, pDepthStencilView);
	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	pImmediateContext->GSSetShader(NULL, NULL, 0);
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Render scene

	Vector position = player->GetEyePos();

	ConstantBuffer constantbuffer;
	constantbuffer.World = XMMatrixTranspose(l_world);
	constantbuffer.View = XMMatrixTranspose(l_view);
	constantbuffer.Projection = XMMatrixTranspose(l_Projection);
	constantbuffer.LightViewProjection = XMMatrixTranspose(XMMatrixIdentity());
	constantbuffer.info = XMFLOAT4(curr_maxlevel, 1, 1, 1);
	constantbuffer.CameraPos = XMFLOAT4(position.x, position.y, position.z, 1);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &constantbuffer, 0, 0);

	pImmediateContext->VSSetShader(pVFLVS, NULL, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);

	pImmediateContext->PSSetShader(pDeferredPS, NULL, 0);
	pImmediateContext->PSSetSamplers(0, 1, &pSamplerLinear);

	render(pImmediateContext, 0, pCBuffer, constantbuffer);

	pSwapChain->Present(0, 0);

	ID3D11ShaderResourceView* srvs[D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
	pImmediateContext->VSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->PSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->GSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);

	
	}
//**********************************************************************************************
void vxgi_::render_7_of_8_deferred_shadowing(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection,  shadow_mapping_* shadow_mapping)
	{
	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;

	float ClearColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f }; // red, green, blue, alpha
	ID3D11RenderTargetView*			RenderTarget;
	RenderTarget = DeferredShadow.GetRenderTargetView();

	// Clear render target & shaders, set render target & primitive topology
	pImmediateContext->ClearRenderTargetView(RenderTarget, ClearColor);
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	pImmediateContext->OMSetRenderTargets(1, &RenderTarget, pDepthStencilView);
	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	pImmediateContext->GSSetShader(NULL, NULL, 0);
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// Render scene

	ID3D11ShaderResourceView* Positions_SRV = Positions.GetShaderResourceView();
	ID3D11ShaderResourceView* shadow_texture = shadow_mapping->get_output()->GetShaderResourceView();
	pImmediateContext->PSSetShaderResources(0, 1, &Positions_SRV);
	pImmediateContext->PSSetShaderResources(1, 1, &shadow_texture);

	Vector position = player->GetEyePos();
	
	ID3D11SamplerState* pointsampler = shadow_mapping->get_point_sampler();
	pImmediateContext->PSSetSamplers(1, 1, &pointsampler);
	ConstantBuffer constantbuffer;
	constantbuffer.World = XMMatrixTranspose(l_world);
	constantbuffer.View = XMMatrixTranspose(l_view);
	constantbuffer.Projection = XMMatrixTranspose(l_Projection);
	constantbuffer.LightViewProjection = XMMatrixTranspose(shadow_mapping->get_light_VP());
	constantbuffer.info = XMFLOAT4(curr_maxlevel, 1, 1, 1);
	constantbuffer.CameraPos = XMFLOAT4(position.x, position.y, position.z, 1);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &constantbuffer, 0, 0);
	
	pImmediateContext->VSSetShader(pSceneScreenVS, NULL, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShader(pDeferredShadowPS, NULL, 0);
	pImmediateContext->PSSetSamplers(0, 1, &pSamplerLinear);
	pImmediateContext->PSSetShaderResources(2, 1, &flashlightMask);
	pImmediateContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);
	pImmediateContext->Draw(6, 0);
	pSwapChain->Present(0, 0);
	ID3D11ShaderResourceView* srvs[D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
	pImmediateContext->VSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->PSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->GSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);


	}
//**********************************************************************************************
extern int mode;
void vxgi_::render_8_of_8_cone_tracing(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection,  shadow_mapping_* shadow_mapping, bool draw_voxels)
	{
	if (draw_voxels)
		{
		render_voxels(pImmediateContext, l_view, l_Projection);
		return;
		}

	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;
	
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // red, green, blue, alpha
	ID3D11RenderTargetView*			RenderTarget;
	RenderTarget = ScreenOutput.GetRenderTargetView();

	// Clear render target & shaders, set render target & primitive topology
	pImmediateContext->ClearRenderTargetView(RenderTarget, ClearColor);
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	pImmediateContext->OMSetRenderTargets(1, &RenderTarget, pDepthStencilView);
	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	pImmediateContext->GSSetShader(NULL, NULL, 0);
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Render scene
	//from shadowmap:
	ID3D11ShaderResourceView* shadow_texture = shadow_mapping->get_output()->GetShaderResourceView();
	pImmediateContext->PSSetShaderResources(3, 1, &shadow_texture);
	ID3D11SamplerState* pointsampler = shadow_mapping->get_point_sampler();
	pImmediateContext->PSSetSamplers(1, 1, &pointsampler);

	Vector position = player->GetEyePos();

	ConstantBuffer constantbuffer;
	constantbuffer.World = XMMatrixTranspose(l_world);
	constantbuffer.View = XMMatrixTranspose(l_view);
	constantbuffer.Projection = XMMatrixTranspose(l_Projection);
	constantbuffer.LightViewProjection = XMMatrixTranspose(shadow_mapping->get_light_VP());
	constantbuffer.info = XMFLOAT4(curr_maxlevel, mode, 1, 1);
	constantbuffer.CameraPos = XMFLOAT4(position.x, position.y, position.z,1);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &constantbuffer, 0, 0);

	ID3D11ShaderResourceView* Octree_SRV = Octree.GetShaderResourceView();
	ID3D11ShaderResourceView* BrickBuffer_SRV = BrickBuffer.GetShaderResourceView();
	
	ID3D11ShaderResourceView* Positions_SRV = Positions.GetShaderResourceView();
	ID3D11ShaderResourceView* Normals_SRV = Normals.GetShaderResourceView();
	ID3D11ShaderResourceView* Tangents_SRV = Tangents.GetShaderResourceView();
	ID3D11ShaderResourceView* DeferredShadow_SRV = DeferredShadow.GetShaderResourceView();	
	ID3D11ShaderResourceView* Diffuse_SRV = Diffuse.GetShaderResourceView();
	
	pImmediateContext->PSSetShaderResources(1, 1, &Octree_SRV);
	pImmediateContext->PSSetShaderResources(2, 1, &BrickBuffer_SRV);
	
	pImmediateContext->PSSetShaderResources(4, 1, &Positions_SRV);
	pImmediateContext->PSSetShaderResources(5, 1, &Normals_SRV);
	pImmediateContext->PSSetShaderResources(6, 1, &Tangents_SRV);
	pImmediateContext->PSSetShaderResources(7, 1, &Diffuse_SRV);
	pImmediateContext->PSSetShaderResources(8, 1, &DeferredShadow_SRV);

	pImmediateContext->VSSetShader(pSceneScreenVS, NULL, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);

	pImmediateContext->PSSetShader(pScenePS, NULL, 0);
	pImmediateContext->PSSetSamplers(0, 1, &pSamplerLinear);

	pImmediateContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);
	pImmediateContext->Draw(6, 0);

	pSwapChain->Present(0, 0);

	ID3D11ShaderResourceView* srvs[D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
	pImmediateContext->VSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->PSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->GSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);


	}

void vxgi_::render_comparison(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection, shadow_mapping_* shadow_mapping, bool draw_voxels)
{
	if (draw_voxels)
	{
		render_voxels(pImmediateContext, l_view, l_Projection);
		return;
	}

	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;

	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // red, green, blue, alpha
	ID3D11RenderTargetView*			RenderTarget;
	RenderTarget = ScreenOutput.GetRenderTargetView();

	// Clear render target & shaders, set render target & primitive topology
	pImmediateContext->ClearRenderTargetView(RenderTarget, ClearColor);
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	pImmediateContext->OMSetRenderTargets(1, &RenderTarget, pDepthStencilView);
	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	pImmediateContext->GSSetShader(NULL, NULL, 0);
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Render scene
	//from shadowmap:
	ID3D11ShaderResourceView* shadow_texture = shadow_mapping->get_output()->GetShaderResourceView();
	pImmediateContext->PSSetShaderResources(3, 1, &shadow_texture);
	ID3D11SamplerState* pointsampler = shadow_mapping->get_point_sampler();
	pImmediateContext->PSSetSamplers(1, 1, &pointsampler);

	Vector position = player->GetEyePos();

	ConstantBuffer constantbuffer;
	constantbuffer.World = XMMatrixTranspose(l_world);
	constantbuffer.View = XMMatrixTranspose(l_view);
	constantbuffer.Projection = XMMatrixTranspose(l_Projection);
	constantbuffer.LightViewProjection = XMMatrixTranspose(shadow_mapping->get_light_VP());
	constantbuffer.info = XMFLOAT4(curr_maxlevel, mode, 1, 1);
	constantbuffer.CameraPos = XMFLOAT4(position.x, position.y, position.z, 1);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &constantbuffer, 0, 0);

	ID3D11ShaderResourceView* Octree_SRV = Octree.GetShaderResourceView();
	ID3D11ShaderResourceView* BrickBuffer_SRV = BrickBuffer.GetShaderResourceView();

	ID3D11ShaderResourceView* Positions_SRV = Positions.GetShaderResourceView();
	ID3D11ShaderResourceView* Normals_SRV = Normals.GetShaderResourceView();
	ID3D11ShaderResourceView* Tangents_SRV = Tangents.GetShaderResourceView();
	ID3D11ShaderResourceView* DeferredShadow_SRV = DeferredShadow.GetShaderResourceView();
	ID3D11ShaderResourceView* Diffuse_SRV = Diffuse.GetShaderResourceView();

	pImmediateContext->PSSetShaderResources(1, 1, &Octree_SRV);
	pImmediateContext->PSSetShaderResources(2, 1, &BrickBuffer_SRV);

	pImmediateContext->PSSetShaderResources(4, 1, &Positions_SRV);
	pImmediateContext->PSSetShaderResources(5, 1, &Normals_SRV);
	pImmediateContext->PSSetShaderResources(6, 1, &Tangents_SRV);
	pImmediateContext->PSSetShaderResources(7, 1, &Diffuse_SRV);
	pImmediateContext->PSSetShaderResources(8, 1, &DeferredShadow_SRV);

	pImmediateContext->VSSetShader(pSceneScreenVS, NULL, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);

	pImmediateContext->PSSetShader(g_pScreenPScompare, NULL, 0);
	pImmediateContext->PSSetSamplers(0, 1, &pSamplerLinear);

	pImmediateContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);
	pImmediateContext->Draw(6, 0);

	pSwapChain->Present(0, 0);

	ID3D11ShaderResourceView* srvs[D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
	pImmediateContext->VSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->PSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->GSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);


}

void vxgi_::render_voxels(ID3D11DeviceContext* pImmediateContext, XMMATRIX &l_view, XMMATRIX &l_Projection)
	{
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // red, green, blue, alpha
	ID3D11RenderTargetView*			RenderTarget;
	RenderTarget = ScreenOutput.GetRenderTargetView();

	// Clear render target & shaders, set render target & primitive topology
	pImmediateContext->ClearRenderTargetView(RenderTarget, ClearColor);
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	pImmediateContext->OMSetRenderTargets(1, &RenderTarget, pDepthStencilView);
	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	pImmediateContext->GSSetShader(NULL, NULL, 0);

	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;

	ConstantBuffer cb;
	cb.World = XMMatrixTranspose(XMMatrixIdentity());
	cb.View = XMMatrixTranspose(l_view);
	cb.Projection = XMMatrixTranspose(l_Projection);
	cb.info.x = 1;

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);

	pImmediateContext->VSSetShader(pVoxelVS, NULL, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->VSSetSamplers(0, 1, &pSamplerLinear);

	ID3D11ShaderResourceView* count_SRV = const_count.GetShaderResourceView();
	ID3D11ShaderResourceView* Octree_SRV = Octree.GetShaderResourceView();

	pImmediateContext->GSSetShader(pVoxelGS, NULL, 0);
	pImmediateContext->GSSetShaderResources(0, 1, &Octree_SRV);
	pImmediateContext->GSSetShaderResources(1, 1, &count_SRV);
	pImmediateContext->GSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->GSSetSamplers(0, 1, &pSamplerLinear);

	pImmediateContext->PSSetShader(pVoxelPS, NULL, 0);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetSamplers(0, 1, &pSamplerLinear);

	pImmediateContext->IASetVertexBuffers(0, 1, &voxel.vbuffer, &stride, &offset);


	pImmediateContext->Draw(voxel.anz, 0);

	count_SRV = 0;
	Octree_SRV = 0;

	ID3D11ShaderResourceView* srvs[D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
	pImmediateContext->VSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->PSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	pImmediateContext->GSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	}